

import java.util.Arrays;

/**
 *
 * @author fa
 */
public class saludo {

    public static void main(String[] args) {

        String lines = "hola mundo";
      
        String[] result = Arrays.stream(lines.split("\\s+"))
			.map(String::toUpperCase)
			.toArray(String[]::new);

        for (String s : result) {
            System.out.println(s);
        }

    }

}