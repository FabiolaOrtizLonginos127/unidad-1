#include <stdio.h>
#include <stdlib.h>

#define TAM 10

struct dir{
	char nombre[30];
	char calle[40];
	char ciudad[20];
	char estado[3];
	unsigned long int codigo;
}binfo, cinfo;

struct {
	unsigned long int codigo;
}una_sola_variable;

struct dir info_dir;
struct dir info_dir_dos[TAM];

int main(int argc, char const *argv[]){	
	register int t;	
	info_dir.codigo = 12345;
	binfo.codigo = 12346;
	cinfo.codigo = 12347;	
	una_sola_variable.codigo = 12348;
	info_dir_dos[0].codigo = 10;
	info_dir_dos[1].codigo = 11;
	info_dir_dos[2].codigo = 12;
	printf("%ld\n", info_dir.codigo);
	printf("%ld\n", binfo.codigo);
	printf("%ld\n", cinfo.codigo);
	printf("%ld\n", una_sola_variable.codigo);
	for(t = 0; t < 3; ++t){
          
		printf("%ld\n", info_dir_dos[t].codigo);
	}
	printf("Introduce tu Nombre: \n");
	gets(info_dir.nombre);
	
    printf("Tu nombre es: %s\n", info_dir.nombre);
	// acceder a caracteres individuales
       for(t = 0; info_dir.nombre[t]; ++t){
		putchar(info_dir.nombre[t]);
	    printf("%c", info_dir.nombre[t]);
	}
	printf("\n");
	return 0;
}
